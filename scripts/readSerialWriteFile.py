import time
import datetime
import serial

ser = serial.Serial(
    port='/dev/ttyACM0',
    baudrate=115200,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.SEVENBITS
)
if not ser.isOpen() :
    ser = serial.Serial(
        port='/dev/ttyACM1',
        baudrate=115200,
        parity=serial.PARITY_ODD,
        stopbits=serial.STOPBITS_TWO,
        bytesize=serial.SEVENBITS
    )
    if not ser.isOpen() :
        ser = serial.Serial(
            port='/dev/ttyACM2',
            baudrate=115200,
            parity=serial.PARITY_ODD,
            stopbits=serial.STOPBITS_TWO,
            bytesize=serial.SEVENBITS
        )


nMax = 300
t0 = time.time()
d0 = datetime.datetime.fromtimestamp(t0).strftime('%Y-%m-%d-%H:%M:%S')
filename = 'data' + '-' + d0 + '.csv'
f = open(filename,'w')
f.write("id , solar , wind , hydro \n")

n = 0
while n < nMax:
    data = ser.readline()
    datats = str(int(time.time())) + " , " + data
    f.write(datats)
    n = n+1
    print nMax - n
f.close()




