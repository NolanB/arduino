/* wattsimuno */
#include <Arduino.h>
#include <LiquidCrystal.h>
#include <OneWire.h>
#include <Wire.h>
#include <Adafruit_INA219.h>

Adafruit_INA219 ina219_0x40;
Adafruit_INA219 ina219_0x41(0x41);
const int N0 = 5;
float current_mA0_tab[N0];
float current_mA1_tab[N0];
float power_mW0_tab[N0];
float power_mW1_tab[N0];

LiquidCrystal lcd(12,11,5,4,3,2);

const int sensorPin0 = A0;
const int sensorPin1 = A1;
const int sensorPin2 = A2;
const int sensorPin3 = A3;
const int sensorPin4 = A4;
const float baselineTemp = 20.0;

/* Broche du bus 1-Wire */
const byte BROCHE_ONEWIRE = 7;

/* Code de retour de la fonction getTemperature() */
enum DS18B20_RCODES {
  READ_OK,  // Lecture ok
  NO_SENSOR_FOUND,  // Pas de capteur
  INVALID_ADDRESS,  // Adresse reçue invalide
  INVALID_SENSOR  // Capteur invalide (pas un DS18B20)
};


/* Création de l'objet OneWire pour manipuler le bus 1-Wire */
OneWire ds(BROCHE_ONEWIRE);
 
 
/**
 * Fonction de lecture de la température via un capteur DS18B20.
 */
byte getTemperature(float *temperature, byte reset_search) {
  byte data[9], addr[8];
  // data[] : Données lues depuis le scratchpad
  // addr[] : Adresse du module 1-Wire détecté
  
  /* Reset le bus 1-Wire ci nécessaire (requis pour la lecture du premier capteur) */
  if (reset_search) {
    ds.reset_search();
  }
 
  /* Recherche le prochain capteur 1-Wire disponible */
  if (!ds.search(addr)) {
    // Pas de capteur
    return NO_SENSOR_FOUND;
  }
  
  /* Vérifie que l'adresse a été correctement reçue */
  if (OneWire::crc8(addr, 7) != addr[7]) {
    // Adresse invalide
    return INVALID_ADDRESS;
  }
 
  /* Vérifie qu'il s'agit bien d'un DS18B20 */
  if (addr[0] != 0x28) {
    // Mauvais type de capteur
    return INVALID_SENSOR;
  }
 
  /* Reset le bus 1-Wire et sélectionne le capteur */
  ds.reset();
  ds.select(addr);
  
  /* Lance une prise de mesure de température et attend la fin de la mesure */
  ds.write(0x44, 1);
  delay(800);
  
  /* Reset le bus 1-Wire, sélectionne le capteur et envoie une demande de lecture du scratchpad */
  ds.reset();
  ds.select(addr);
  ds.write(0xBE);
 
 /* Lecture du scratchpad */
  for (byte i = 0; i < 9; i++) {
    data[i] = ds.read();
  }
   
  /* Calcul de la température en degré Celsius */
  *temperature = ((data[1] << 8) | data[0]) * 0.0625; 
  
  // Pas d'erreur
  return READ_OK;
}
 

void setup() {
  // lcd initialization
  lcd.begin(16,2);
  lcd.print("Watt'SIM");

  for(int pinNumber = 2; pinNumber<5; pinNumber++) {
    pinMode(pinNumber, OUTPUT);
    digitalWrite(pinNumber, LOW);
  }
  /* Initialisation du port série */
  Serial.begin(115200);
  while (!Serial) {
      // will pause Zero, Leonardo, etc until serial console opens
      delay(1);
  }

  uint32_t currentFrequency;
    
  Serial.println("Hello!");
  
  // Initialize the INA219.
  // By default the initialization will use the largest range (32V, 2A).  However
  // you can call a setCalibration function to change this range (see comments).
  //ina ina219_0x40.begin();
  //ina ina219_0x41.begin();
  // To use a slightly lower 32V, 1A range (higher precision on amps):
  //ina219_0x40.setCalibration_32V_1A();
  //ina219_0x41.setCalibration_32V_1A();
  // Or to use a lower 16V, 400mA range (higher precision on volts and amps):
  //ina219_0x40.setCalibration_16V_400mA();
  //ina219_0x41.setCalibration_16V_400mA();

  for(int i=0; i<N0; i++) { current_mA0_tab[i] = 0.0; }
  for(int i=0; i<N0; i++) { current_mA1_tab[i] = 0.0; }
  for(int i=0; i<N0; i++) { power_mW0_tab[i] = 0.0; }
  for(int i=0; i<N0; i++) { power_mW1_tab[i] = 0.0; }
  
  //ina Serial.println("Measuring voltage and current with INA219 ...");  
}

void loop() {
  // put your main code here, to run repeatedly:

  int sensorVal0 = analogRead(sensorPin0);
  int sensorVal1 = analogRead(sensorPin1);
  int sensorVal2 = analogRead(sensorPin2);
  int sensorVal3 = analogRead(sensorPin3);
  int sensorVal4 = analogRead(sensorPin4);
  //Serial.print("Sensor Value: ");
  //Serial.print(sensorVal0);
  float v_solar = (sensorVal1/1024.0) * 5.0;
  float v_wind = (sensorVal2/1024.0) * 5.0;
  float v_hydro = (sensorVal3/1024.0) * 5.0;
  float v_temperature = (sensorVal4/1024.0) * 5.0;
  //Serial.print(", Volts: ");
  //Serial.print(v_temperature);
  float temperature = (v_temperature - .5) * 100;
  //Serial.print(", Temperature: ");
  //Serial.print(temperature);
  //Serial.print(",");

  float shuntvoltage = 0;
  float busvoltage = 0;
  float current_mA0 = 0;
  float current_mA1 = 0;
  float loadvoltage = 0;
  float power_mW0 = 0;
  float power_mW1 = 0;

  //ina shuntvoltage = ina219_0x40.getShuntVoltage_mV();
  //ina busvoltage = ina219_0x40.getBusVoltage_V();
  //ina current_mA0 = ina219_0x40.getCurrent_mA();
  //ina power_mW0 = ina219_0x40.getPower_mW();
  loadvoltage = busvoltage + (shuntvoltage / 1000);
  
  //Serial.print("Bus Voltage:   "); Serial.print(busvoltage); Serial.println(" V");
  //Serial.print("Shunt Voltage: "); Serial.print(shuntvoltage); Serial.println(" mV");
  //Serial.print("Load Voltage:  "); Serial.print(loadvoltage); Serial.println(" V");
  //Serial.print("Current:       "); Serial.print(current_mA); Serial.println(" mA");
  //Serial.print("Power:         ");
  //power_mW0 = 10.0;
  for (int i=N0-1; i>0; i--) { current_mA0_tab[i] = current_mA0_tab[i-1]; }
  current_mA0 = current_mA0;
  current_mA0_tab[0] = current_mA0;
  for (int i=N0-1; i>0; i--) { power_mW0_tab[i] = power_mW0_tab[i-1]; }
  power_mW0 = power_mW0 - 14.3;
  power_mW0_tab[0] = power_mW0;

  float mean = 0;
  for (int i=0; i<N0; i++) {mean = mean + current_mA0_tab[i]; }
  current_mA0 = mean / N0;
  mean = 0;
  for (int i=0; i<N0; i++) {mean = mean + power_mW0_tab[i]; }
  power_mW0 = mean / N0;
  //Serial.println("");
  //Serial.println(mean); 
  //Serial.print(current_mA0); 
  //ina Serial.print(power_mW0); 
  //Serial.println(" mW");
  //ina Serial.print(",");

  //ina shuntvoltage = ina219_0x41.getShuntVoltage_mV();
  //ina busvoltage = ina219_0x41.getBusVoltage_V();
  //ina current_mA1 = ina219_0x41.getCurrent_mA();
  //ina power_mW1 = ina219_0x41.getPower_mW();
  loadvoltage = busvoltage + (shuntvoltage / 1000);
  
  //Serial.print("Bus Voltage:   "); Serial.print(busvoltage); Serial.println(" V");
  //Serial.print("Shunt Voltage: "); Serial.print(shuntvoltage); Serial.println(" mV");
  //Serial.print("Load Voltage:  "); Serial.print(loadvoltage); Serial.println(" V");
  //Serial.print("Current:       "); Serial.print(current_mA); Serial.println(" mA");
  //Serial.print("Power:         "); 
  for (int i=N0-1; i>0; i--) { current_mA1_tab[i] = current_mA1_tab[i-1]; }
  current_mA1_tab[0] = current_mA1;
  for (int i=N0-1; i>0; i--) { power_mW1_tab[i] = power_mW1_tab[i-1]; }
  power_mW1_tab[0] = power_mW1;

  mean = 0;
  for (int i=0; i<N0; i++) {mean = mean + current_mA1_tab[i]; }
  current_mA1 = mean / N0; 
  mean = 0;
  for (int i=0; i<N0; i++) {mean = mean + power_mW1_tab[i]; }
  power_mW1 = mean / N0; 
  //Serial.print(current_mA1); 
  //ina Serial.print(power_mW1); 
  //ina Serial.print(",");
  //ina Serial.print(power_mW1); 
  //Serial.println(" mW");
  //ina Serial.println("");

  //lcd.setCursor(0,1);
  lcd.setCursor(10,0);
  //lcd.print("T:");
  //lcd.print(temperature);
  lcd.print("S:");
  lcd.print(v_solar);
  Serial.print(v_solar);
  Serial.print(",");
  lcd.setCursor(0,1);
  lcd.print(" W:");
  lcd.print(v_wind);
  Serial.print(v_wind);
  Serial.print(",");
  lcd.print(" H:");
  lcd.print(v_hydro);
  Serial.print(v_hydro);
  Serial.println("");
  /*
  / Lit la température ambiante à ~1Hz /
  if (getTemperature(&temperature, true) != READ_OK) {
    Serial.println(F("Erreur de lecture du capteur"));
    return;
  }

  / Affiche la température /
  Serial.print(F("Temperature : "));
  Serial.print(temperature, 2);
  Serial.write(176); // Caractère degré
  Serial.write('C');
  Serial.println();
  */
  
  delay(3000);
}                                                                                                                                                                                                                                      
