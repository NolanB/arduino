# Watt'SIM

[Le concept en images](http://ramparany.free.fr/wattsim/test2/img0.html)  

Le saviez vous ?: _"les ressources énergétiques de notre planète seraient sur le point de s'épuiser"_.  
Un autre secret ?: _"Les enfants d'aujourd'hui sont les adultes de demain !"_  
Un dernier secret : _"après 2 semaines, on retient 10% de ce qu'on a lu; on retient 90% de ce qu'on a dit et expérimenté"_  

Pour réussir à sensibiliser les futurs adultes à l'utilisation des ressources energétiques renouvelables, nous avons imaginé pour eux un jeu éducatif: Watt'SIM
".  

[_...en savoir plus_](http://ramparany.free.fr/wattsim/intro.html)

## La maquette et l'application Watt'SIM

[_voir une petite vidéo_](https://youtu.be/Xw1ra5GWs58)

En plein écran:

<figure class="video_container">
  <iframe src="" frameborder="0" allowfullscreen="true">https://www.youtube.com/embed/Xw1ra5GWs58 </iframe>
</figure>

[_...en savoir plus_](http://ramparany.free.fr/wattsim/maquette.html)

## L'équipe

*   **Aude**
*   **Fano**
*   **Erwan**

[_...en savoir plus_](http://ramparany.free.fr/wattsim/equipe.html)

